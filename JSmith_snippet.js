// Code sample from Jaclyn Smith
// Subset of editgraph.js of the skti ontology alignment generator
// Mapping matrix, button UI, and user input consolidation of class/attribute cardinality
// Developed through the AIM lab 2014
// current version can be found: http://aimlab.cs.uoregon.edu/services/skti-ontogen/


  /* MAPPING-MATRIX INITIALIZATION */
  // mapping rule key/value classes  
  // os T1 -> ot Ax & P
  function classEnumToProp(osA, osT1){
	  this.osA = osA;
	  this.osT1 = osT1;
	  this.INFs = [];
	  var that = this;
	  
	  this.addINF = function (Ts, P){
		  var i = 1;
		  Ts.forEach(function (T){
			  var INF = {"left" : {}, "right" : {"otA" : that.osA, "otP" : P}};
			  INF["left"]["osT"+i] = T;
			  that.INFs.push(INF);
			  i++;
		  });
	  };
	  
	  this.getKey = function() {
		  return (that.osA, that.osT1);
	  };
	  
	  this.getValue = function() {
		  return that.INFs;
	  };
	 
  }
  
  function attributeToSubclass(osA, P1){
	  this.osA = osA;
	  this.P = P1;
	  this.INFs = [];
	  var that = this;
	  
	  this.addINF = function (Ts, Ps){
		  var c = 1;
		  Ts.forEach(function (T){
			  var INF = {"left" : {"osA" : that.osA}, "right" : {}};
			  INF["right"]["otT"+c] = T;
			  var i = 1;
			  Ps.forEach(function (P){
				  INF["left"]["osP"+i] = P;
				  i++;
			  });
			  that.INFs.push(INF);
			  c++;
		  });
	  };
	  
	  this.getKey = function() {
		  return (that.osA, that.P);
	  };
	  
	  this.getValue = function() {
		  return that.INFs;
	  };
  }
  
 //@OS :A(x) ^ B(y) ^ C(z) ^ p1(x; y) ^ p2(y; z)
 //@OT :p3(x; z)
  function defineTrans(p1, p2) {
	  this.p1 = p1;
	  this.p2 = p2;
	  this.INF = {};
	  var that = this;
	  

	  this.addINF = function (A, B, C, p3) {
		  that.INF["left"] = {"osA" : A, "osB" : B, "osC" : C, "osP1" : that.p1, "osP2" : that.p2};
		  that.INF["right"]= {"otP3" : p3};
	  };
	  
	  this.getKey = function() {
		  return that.p1 + "" + that.p2; //p1, p2 are strings
	  };
	  
	  this.getValue = function() {
		  return that.INF;
	  };
  }
  
  //@OS :A(x) ^ C(z) ^ p1(x; z) !
  //@OT :B(y) ^ p2(x; y) ^ p3(y; z)
  function decomposeTrans(p) {
	  this.p = p;
	  this.INF = {};
	  var that = this;
	  

	  this.addINF = function (A, B, C, p2, p3) {
		  that.INF["left"] = {"osA" : A, "osC" : C, "osP1" : that.p};
		  that.INF["right"]= {"otB" : B, "otP2" : p2, "otP3" : p3};
	  };
	  
	  this.getKey = function() {
		  return that.p;
	  };
	  
	  this.getValue = function() {
		  return that.INF;
	  };
  }

    // mapping rule object matrix
  mapping_matrix = {
    // complex class deletion
    "DelClassKeepAll": {},
    "DelClassKeepSub": {}, 
    
    // categorization mismatch
    
    "CatMismatch1N" : {},
    "CatMismatchN1" : {},
    
    // functional transformation
    
    "FuncTran1to1" : {},
    "FuncTran1toN" : {},
    "FuncTranNto1" : {},
    
    // class/attribute heterogeniety
    "ClassEnumToProp"	: {},
    "AttToSubclass"		: {},
    "DefineTrans" 		: {},
    "DecomposeTrans" 	: {}
  };

  function add_node(name, parent) {
      // check user input for a valid name and handle new name input
      if (name.indexOf(':') != -1) {
        alert('Please enter a class name that does not contain a : character.')
        return false;
      }
    
      var supercls_node = get_node(parent);
      var depth = supercls_node.depth + 1;
      var new_node = create_node(name, depth, supercls_node.id);
      var new_link = create_subclass_link(supercls_node, new_node);
      links.push(new_link);

      // update matrix
      var i_tmp = get_matrix_indices(new_link);
      matrix[i_tmp.source][i_tmp.target].subclass.add += 1;

      update();
      $("#info").remove();  // finished processing, remove the info box
      // save state after new changes
      save_state();
      made_change = true;
      return false;
    }

  function add_property_portable(prop_name, domain_name, range_name) {
    if (prop_name == "") {
      alert("Property name is required.");
      return false;
    }
    if (prop_name.indexOf(':') != -1) {
      alert('Please enter a class name that does not contain a : character.')
      return false;
    }
    var domain_node = get_node(domain_name);
    var range_node = get_node(range_name);

    // verify that this link does not already exist
    if (check_for_existing_link(prop_name, domain_name, range_name)) {
      alert('A property with the same name, domain, and range already exists in the ontology model.');
      return false;
    }
          // create and add property link
    var new_link = create_property_link(prop_name, domain_node, range_node);
    links.push(new_link);
    property_links.push(new_link);

    // update matrix
    add_matrix_property(new_link);

    return false;
  }

  //portable function: delete a property for complex buttons
  function delete_property_portable(propertyName) {
      var del_link;
      links.forEach(function(link){
        if(propertyName == link.propname){
          del_link = link;
        }
      });
      property_links.splice(property_links.indexOf(del_link), 1);
      links.splice(links.indexOf(del_link), 1);
  }



  function class_attribute_cardinality_change() {

    // handle changes upon <form> submit
    function class_attribute_cardinality_helper() {
      
      var logic_object = {};
      var classAttLogic;
      //CLASS ATTR enumeration
      if($('#enum').is(':checked')){
    	  if($('#enumToProp').is(':checked')){
    		  if($('#textInput').val() == ''){
    			  alert('Must enter property name.');
    			  return false;
    		  }
    		  var parent_name = $('#parMen :selected').text();
        	  var TsArr = $(document.getElementsByClassName('radioLike')).filter(':checked');
        	  TsArr = $.map(TsArr, function (element, i) {
        		  return $(element).parent().text();
        	  });
        	  if(TsArr.length < 1){
        		  alert('At least one node must be selected.');
        		  return false;
        	  }
        	  var prop = $('#textInput').val();
        	  logic_object = new classEnumToProp(parent_name, TsArr[0]);
        	  logic_object.addINF(TsArr, prop);
        	  for(var i = 0; i < TsArr.length; i++){
        		  delete_class_portable(TsArr[i]);
        	  }
        	  add_property_portable(prop, parent_name, 'xsd:string');
        	  
        	  classAttLogic = mapping_matrix["ClassEnumToProp"];
        	//CLASS/PROPERTY RELATION CHANGE: property to enumeration
    	  }else if($('#propToEnum').is(':checked')){
    		  if(transportList.length < 1){
    			  alert('Must change property to at least one subchild.');
    			  $('#textInput').focus();
    			  return false;
    		  }
    		  var source_name = $('#sourceMen :selected').text();
    		  
    	   	  var propNames = [];
        	  //saving target names for visuals
    	   	  var targetNames = [];
        	  var psArr = $(document.getElementsByClassName('radioLike')).filter(':checked');
        	  psArr = $.map(psArr, function (element, i) {
        		  return $(element).parent().text().split(' : ');
        	  });
        	  for(var i = 0; i < psArr.length; i += 2){
        		  propNames.push(psArr[i]);
        		  targetNames.push(psArr[i+1]);
        	  }
        	  if(propNames.length < 1){
        		  alert('Must select at least one property.');
        		  return false;
        	  }
    		  logic_object = new attributeToSubclass(source_name, propNames[0]);
    		  logic_object.addINF(transportList, propNames);

    		  for(var i = 0; i < propNames.length; i++){
        		  delete_property_portable(propNames[i]);
        	  }
    		  
    		  //transport list is a global array, edited inside UI implementation
    		  //and used on button submit functionality
    		  transportList.forEach(function (child) {
    			 add_node(child, source_name);
    		  });
    		  
    	  }
      //CHANGE PROPERTY TRANSITIVITY: decompose transitivity
      }else if($('#attr').is(':checked')){
    	  if($('#decompTrans').is(':checked')){
    		  
    		  if($('#nodesMenu').val() == '--Select Midpoint Node--'){
    			  alert('All drop downs must be selected.');
    			  return false;
    		  };
    		  if($('#p1text').val() == ''){
    			  alert('Must provide a name for property 1.');
    			  $('#p1text').focus();
    			  return false;
    		  }
    		  if($('#p2text').val() == ''){
    			  alert('Must provide a name for property 2.');
    			  $('#p2text').focus();
    			  return false;
    		  }
    		  var p = $('#propertiesMenu').val().split(' : ')[0];
    		  logic_object = new decomposeTrans(p);
    		  var A = $('#sourceNodes').val();
    		  var B = $('#nodesMenu').val();
    		  var C;
    		  property_links.forEach(function (link) {
    		  if (link.propname == $('#propertiesMenu').val().split(' : ')[0])
    				C = link.target.name;
    		  })
    		  var p2 = $('#p1text').val(); 
    		  var p3 = $('#p2text').val();
    		  logic_object.addINF(A, B, C, p2, p3);

    		  classAttLogic = mapping_matrix["DecomposeTrans"];
    		  add_property_portable(p2, A, B);
    		  add_property_portable(p3, B, C);
    		  delete_property_portable(p);
    		  
    		//CHANGE PROPERTY TRANSITIVITY: define transitivity
    	  }else if($('#defineTrans').is(':checked')){
    		  if($('#outMenu').val() == '--Select Second Property--'){
    			  alert('All drop downs must be selected.');
    			  return false;
    		  };
    		  if($('#transText').val() == ''){
    			  alert('Must provide a property name.');
    			  $('#transText').focus();
    			  return false;
    		  }
    		  logic_object = new defineTrans($('#inMenu').val().split(' : ')[0], $('#outMenu').val().split(' : ')[0]);
    		  var A = $('#sources1B').val();
    		  var B,C;
    		  property_links.forEach(function (link) {
      		  if (link.propname == $('#inMenu').val().split(' : ')[0])
      				B = link.target.name;
      		  if (link.propname == $('#outMenu').val().split(' : ')[0])
      			  	C = link.target.name;
      		  })
      		  var p = $('#transText').val();
    		  logic_object.addINF(A, B, C, p);
    		  var propOne = $('#inMenu').val().split(' : ')[0];
    		  var propTwo = $('#outMenu').val().split(' : ')[0];
    		  delete_property_portable(propOne);
    		  delete_property_portable(propTwo);
    		  add_property_portable(p, A, C);
    		  classAttLogic = mapping_matrix["DefineTrans"];
    	  }
      }

      classAttLogic[logic_object.getKey()] = logic_object.getValue();
      console.log("Updated Mapping Matrix:", mapping_matrix);
      
      update();
      $("#info").remove();  // finished processing, remove the info box
      // save state after new changes
      save_state();
      made_change = true;
      return false;
    }

    // create #change form within #info svg:g and call custom callback function
    create_change_box("", "Class/Attribute Cardinality Change", "info", class_attribute_cardinality_form, 505, 500);

    // custom callback function to be called after #change form is created
    function class_attribute_cardinality_form(div_tmp) {
      // create input elements and append them to the change form
      var $change = $('#change'); // get the JQuery selector for the change Form

      $change.submit(function() {
        return class_attribute_cardinality_helper();
      });

      // UI Implementation
      //populate drop downs with merged arrays - consequence of collapsing
      var ncopy = $.merge([],nodes);
      var all_nodes = $.merge(ncopy,nodes_stash);
      nodes.forEach(function(node){
    	  if(node.name.indexOf(":") > -1)
    		  all_nodes.splice(all_nodes.indexOf(node), 1);
      });
      var all_links = [];
      links.forEach(function(link) {
    	  if(link.type != "subclass")
    		  all_links.push(link);
      });
      props_stash.forEach(function(link) {
    	 if (link.type != "subclass")
    		 all_links.push(link);
      });      

      $(div_tmp.node()).find('h2').after("<div id='columns' style='width:100%;'></div>");
      $(div_tmp.node()).find('h2').after("<div id='columnsB' style='width:100%;'></div>");
     
      //second column initially hidden for fading
      //each column is associated with a complex change div
      var $columns = $('#columns');
      var $columnsB = $('#columnsB').hide();
     
      var $column1 = $('<div/>')
        .attr('id', 'column1')
        .css({
                width: '50%',
                height: '154px',
                float: 'left',
                margin: 0,
                'margin-bottom': '16px'
        }).appendTo($columns);
    
      var $radio1 = $('<fieldset><legend>Define Action</legend><form></form></fieldset>')
        .css({
        	'margin-bottom': '25px',
        	});
      $columns.before($radio1);
      $columnsB.before($radio1);
     
      var $rad1DivA = $('<div/>').css('margin-bottom', '6px');
      var $enumerationRadio = $('<input/>')
      .attr('type', 'radio')
      .attr('id', 'enum')
      .attr('name', 'misType')
      .attr('checked', 'true')
      .attr('value', 'enumeration').appendTo($rad1DivA);
      $rad1DivA.appendTo($radio1);
    
    $enumerationRadio.after("Class/Property Relation Change<br/>");
    
    var $rad1DivB = $('<div/>');
    var $attributeRadio = $('<input/>')
      .attr('type', 'radio')
      .attr('id', 'attr')
      .attr('name', 'misType')
      .attr('value', 'attribute').appendTo($rad1DivB);
    $rad1DivB.appendTo($radio1);
    
    $attributeRadio.after("Change Property Transitivity");
      
      var $radio2 = $('<fieldset><legend>Choose Direction</legend><form></form></fieldset>')
      	.css({
      		'margin-bottom': '25px',
      	});
      
      var $rad2DivA = $('<div/>').css('margin-bottom', '6px');
      var $enumToProp = $('<input/>')
        .attr('type', 'radio')
        .attr('id', 'enumToProp')
        .attr('name', 'direction')
        .attr('checked', 'true')
        .attr('value', 'Enumeration To Property')
        .change(function () {
        	$sourceMenu.hide();
        	$sourceMenuSpan.hide();
        	$properties.empty().hide();
        	$subclasses.empty().hide(); transportList = [];
        	$('#parMen option:first').attr('selected', true);
        	$parentMenu.show();
        	$parentMenuSpan.show();
        	$children.show();
        	$addButton.hide();
        	$('#legendWords').text('Define Property Relation');
        })
        .appendTo($rad2DivA);
      $rad2DivA.appendTo($radio2)
      
      $enumToProp.after('Enumeration to Property<br/>');
      
      transportList = [];
      
      var $rad2DivB = $('<div/>');
      var $propToEnum = $('<input/>')
      	.attr('type', 'radio')
      	.attr('id', 'propToEnum')
        .attr('name', 'direction')
        .attr('value', 'Property To Enumeration')
        .change(function () {
        	$parentMenu.hide();
        	$parentMenuSpan.hide();
        	$children.empty().hide();
        	$('#sourceMen option:first').attr('selected', true);
        	$sourceMenu.show();
        	$sourceMenuSpan.show();
        	$properties.show();
        	$addButton.show();
        	$subclasses.show();
        	$('#legendWords').text('Instantiate Subclasses')
        	
  			//PREVENT DEFAULT, submit text field to right column - IMPORTANTE NE TOUCHE PAS
  			     $subclasses.append("<script> function deleteItem(toDelete) {$(toDelete).parent().remove(); " +
  		        		            "transportList.forEach(function(a, i) {if($(toDelete).parent().text() == a.toString()+'x')transportList.splice(i,1);});" +
         		                    "return false;}</script>");
        	
  			     $addButton.click(function(event) {
  			         event.preventDefault();
  			         var text = document.getElementById('textInput').value;
  			         if (transportList.indexOf(text) > -1) {
  			        	 alert("Duplicate names cannot be used.");
  			        	 document.getElementById('textInput').value = '';
  			        	 return false;
  			         }
  			         var subtext = text;
  			         if(text != ''){
  			        	 if(text.indexOf(':') != -1){
  			        		 alert('Entry must not contain colons');
  			        	 }else{
  			        	 transportList.push(text);
  			        	 if (text.length > 19) subtext = text.substring(0,19) + '...';
  			            $('<li>' + subtext + '</li>')
  			              .attr('id', text)
  			              .css('border-style', 'solid')
  			              .css('font-family', "Lucida Console")
  			              .css('font-size', "14px")
  			              .css('border-width', '1px')
  			              .css('border-color', '#888888')
  			              .css('width', '194px')
  			              .css('margin-right', '5px')
  			              .css('box-shadow', '2px 2px 4px #888').fadeIn().prependTo($subclasses).append(
  				        	 $('<a/>')
  				        	   .attr('id', 'delButton')
  				        	   .attr('href', 'javascript:void(0)')
  				        	   .attr('onclick', 'deleteItem(this);')
  				        	   .css('float', 'right')
  				        	   .css('float', 'right')
  				        	   .css('position', 'relative')
  				        	   .css('bottom', '3px')
  				        	   .attr('class', 'xbutton').append('x')
  			       	    );
  			        	}
  			       	 	document.getElementById('textInput').value = '';
  			         	}
  			         });

  		  }).appendTo($rad2DivB);
      $rad2DivB.appendTo($radio2);
      $propToEnum.after('Property to Enumeration');
 
      $radio1.after($radio2);
      
      var $radio3 = $('<fieldset><legend>Choose Direction</legend><form></form></fieldset>')
    	.css({
    		'margin-bottom': '25px',
    	}).hide();
      $radio1.after($radio3);
     
     var $rad3DivA = $('<div/>').css('margin-bottom', '6px');
     var $decomposeTrans = $('<input/>')
      .attr('type', 'radio')
      .attr('id', 'decompTrans')
      .attr('name', 'direction2')
      .attr('value', 'Decompose Transitivity').appendTo($rad3DivA);
     $rad3DivA.appendTo($radio3);
     
     var $rad3DivB = $('<div/>');
     var $definePropTrans = $('<input/>')
      .attr('type', 'radio')
      .attr('id', 'defineTrans')
      .attr('name', 'direction2')
      .attr('checked', 'true')
      .attr('value', 'Define Property Transitivity').appendTo($rad3DivB);
     $rad3DivB.appendTo($radio3);
    
    var $column1B = $('<div/>')
      .attr('id', 'column1B')
      .css({
            width: '50%',
            height: '154px',
            float: 'left',
            margin: 0,
            'margin-bottom': '16px'
      }).appendTo($columnsB);
    
    $definePropTrans.change(function() {
    	$('#nodesMenu option:first').attr('selected', true);
    	$('#sourceNodes option:first').attr('selected', true);
    	$('#propertiesMenu option:first').attr('selected', true);
    	$propertiesMenu.empty();
    	$propertiesMenu.append('<option>--Select a Property--</option>');
    	$column1C.hide();
    	$column2C.hide();
    	$column1B.fadeIn();
    	$column2B.fadeIn();
    	$firstProps.attr('disabled', 'true');
    	$secondProps.attr('disabled', 'true');
    	$propertiesMenu.attr('disabled', 'true');
    	$nodesMenu.attr('disabled', 'true');
    });
    
    $definePropTrans.after('Define Property Transitivity<br/>');
    
    $decomposeTrans.change(function() {
    	$('#sources1B option:first').attr('selected', true);
    	$('#inMenu option:first').attr('selected', true);
    	$firstProps.empty();
    	$firstProps.append('<option>--Select First Property--</option>');
    	$firstProps.attr('disabled', 'true');
    	$('#outMenu option:first').attr('selected', true);
    	$secondProps.empty();
    	$secondProps.append('<option>--Select Second Property--</option>');
    	$secondProps.attr('disabled', 'true');
    	$column1B.hide();
    	$column2B.hide();
    	$column1C.fadeIn();
    	$column2C.fadeIn();
    	$firstProps.attr('disabled', 'true');
    	$secondProps.attr('disabled', 'true');
    	$propertiesMenu.attr('disabled', 'true');
    	$nodesMenu.attr('disabled', 'true');
    });
    
    $decomposeTrans.after('Decompose Transitivity');
    
    //construct objects for column1B
    var $sourceNodesMenu1B = $('<select/>')
      .attr('id', 'sources1B')
      .css('width', '190px')
      .css('margin-bottom', '10px')
      .css('margin-top', '6px')
      .appendTo($column1B).change(function() {
    	$firstProps.empty();
    	$secondProps.empty();
    	$firstProps.append('<option>--Select First Property--</option>');
    	$secondProps.append('<option>--Select Second Property--</option');
    	$firstProps.attr('disabled', 'true');
    	$secondProps.attr('disabled', 'true');
    	  //populate firstProps
    	    all_links.forEach(function(link){
    	    	var acceptable = false;
    	    	if (link.source.name == $('#sources1B :selected').text()) {
    	    		all_links.forEach(function (link2) {
    	    			if (link2.source.name == link.target.name) {
	    	    			acceptable = true;
    	    			}
    	    		})
    	    		if (acceptable) {
	    	    		$firstProps.append('<option>'+link.propname+' : '+link.target.name+'</option>');
	    	    		$firstProps.removeAttr('disabled');
    	    		}
    	    	}
    	    });
      });
    $sourceNodesMenu1B.append('<option>--Choose First Source--</option>');
    $sourceNodesMenu1B.before('<span>Choose First Source:</span>');
    
    //populate sourceNodesMenu1B
    all_nodes.forEach(function(node) {
    	var isSource = false;
    	all_links.forEach(function (link) {
    		all_links.forEach(function (link2) {
    			if (link.source.name == node.name && link2.source.name == link.target.name) isSource = true;
    		})
    	});
    	if (isSource) {
    		$sourceNodesMenu1B.append('<option>'+node.name+'</option>');
    	}
    });
    
    var $firstProps = $('<select/>')
      .attr('id', 'inMenu')
      .attr('disabled', 'true')
      .css('margin-bottom', '10px')
      .css('width', '190px')
      .css('margin-top', '6px')
      .change(function () {
    	  $secondProps.empty();
    	  $secondProps.append('<option>--Select Second Property--</option>');
    	  if ($('#inMenu :selected').text() == "--Select First Property--") {
    		  $secondProps.attr('disabled', 'true');
    	  }
    	  //populate secondProps
    	  all_links.forEach(function(link){
  	    	if (link.source.name == $('#inMenu :selected').text().split(" : ")[1]) {
  	    		$secondProps.append('<option>'+link.propname+' : '+link.target.name+'</option>');
  	    		$secondProps.removeAttr('disabled');
  	    	}
  	    });
      })
      .appendTo($column1B);
    $firstProps.append('<option>--Select First Property--</option>');
    $firstProps.before('<span>Select First Property: </span>');
    
    var $secondProps = $('<select/>')
      .attr('id', 'outMenu')
      .attr('disabled', 'true')
      .css('width', '190px')
      .css('margin-top', '6px')
      .appendTo($column1B);
    $secondProps.append('<option>--Select Second Property--</option>');
    $secondProps.before('<span>Select Second Property: </span>');
    
    //div for define property transitivity
    var $column2B = $('<div/>')
    .attr('id', 'column2B')
    .css({
    		width: '50%',
    		height: '154px',
    		margin: 0,
    		'margin-bottom': '16px',
    		float: 'right'
    }).appendTo($columnsB);
    
    $column2B.append('<fieldset><legend>Name Transitive Property</legend><input id="transText" type="text"/></fieldset>');
    
    var $column1C = $('<div/>')
    .attr('id', 'column1C')
    .css({
            width: '50%',
            height: '154px',
            float: 'left',
            margin: 0,
            'margin-bottom': '16px'
    }).hide().appendTo($columnsB);
    
    var $sourceNodesMenu = $('<select/>')
      .attr('id', 'sourceNodes')
      .css('width', '190px')
      .css('margin-bottom', '10px')
      .css('margin-top', '6px')
      .appendTo($column1C);
    $sourceNodesMenu.append('<option>--Select a Source Node--</option>');
    $sourceNodesMenu.before('<span>Select a Source Node:</span>')
    
    var $propertiesMenu = $('<select/>')
      .attr('id', 'propertiesMenu')
      .attr('disabled', 'true')
      .css('width', '190px')
      .css('margin-bottom', '10px')
      .css('margin-top', '6px')
      .change(function () {
    	  if ($('#propertiesMenu :selected').text() == "--Select a Property--") {
    		$('#nodesMenu option:first').attr('selected', true);
    		$nodesMenu.attr('disabled', 'true');
      	} else {
      		$nodesMenu.removeAttr('disabled');
      	}
      })
      .appendTo($column1C);
    $propertiesMenu.append('<option>--Select a Property--</option>');
    $propertiesMenu.before('<span>Select a Property:</span>');
    
    var $nodesMenu = $('<select/>')
      .attr('id', 'nodesMenu')
      .css('width', '190px')
      .css('margin-top', '6px')
      .change(function() {
    	  var this_parent;
    	  if ($('#nodesMenu option:selected').text() != '--Select Midpoint Node--') {
            var node = get_node_ext($('#nodesMenu option:selected').text());
            this_parent = get_parent_ext($('#nodesMenu option:selected').text());
	    	  if(node.collapseVictim){
	    		  var updating_collapsed = node;
	    		  while(updating_collapsed.name != "owl:THING" && updating_collapsed.collapseVictim ) {
	    			  if (updating_collapsed.isCollapsed) {
	    				  updating_collapsed.isCollapsed = false;
	    				  updating_collapsed.fixed = false;
	 	    		  }
	    			  updating_collapsed = get_parent_ext(updating_collapsed.name);
	    		  }
	    		  if (updating_collapsed.isCollapsed && updating_collapsed.name == "owl:THING") {
	    		    updating_collapsed.isCollapsed = false;
	    		    victimize_children(updating_collapsed);
	    		  }else{
	    			 updating_collapsed.isCollapsed = false;
	    			 victimize_children(updating_collapsed);
	    		  }
	    		  update();
	    	  }
    	  if(this_parent != null) d3.select('#importantg').transition().attr("transform", "scale("+2+","+2+") translate("+(350-node.x)+","+(170-node.y)+")");

         }
      })
      .appendTo($column1C);
    $nodesMenu.append('<option>--Select Midpoint Node--</option>');
    $nodesMenu.before('<span>Select Midpoint Node:</span>');
    
    //Populate list of properties.
    //REMEMBER THAT THIS ALLOWS A NODE TO POINT TO ITSELF...
	all_nodes.forEach(function(node) {
		exists = false;
		all_links.forEach(function(link) {
			if(node.name == link.source.name){
				exists = true;
			}
		});
		if(exists){
			$sourceNodesMenu.append('<option>'+ node.name +'</option>');
		}
		if(node.name != 'owl:THING'){
			$nodesMenu.append('<option>'+node.name+'</option>');
		}
	});
	$nodesMenu.attr('disabled', 'true');
    $sourceNodesMenu.change(function(){
    	$propertiesMenu.empty();
    	$('#nodesMenu option:first').attr('selected', true);
    	
    	if ($('#sourceNodes :selected').text() == "--Select a Source Node--") {
        	$nodesMenu.attr('disabled', 'true');
    		$propertiesMenu.attr('disabled', 'true');
    	} else {
    		$propertiesMenu.removeAttr('disabled');
    		$nodesMenu.attr('disabled', 'true');
    	}
    	$propertiesMenu.append('<option>--Select a Property--</option>');
    	all_links.forEach(function(link) {
    		if(link.source.name == $('#sourceNodes :selected').text()){
    			$propertiesMenu.append('<option>'+ link.propname + ' : ' + link.target.name + '</option>');
    		}
    	});
    	all_nodes.forEach(function(node) {
    		exists = false;
    		all_links.forEach(function(link) {
    			if(node.name == link.source.name){
    				exists = true;
    			}
    		});
    		if(exists){
    			$sourceNodesMenu.append('<option>'+ node.name +'</option>');
    		}

    	});
    });
    
    //Change property transitivity: decompose
    var $column2C = $('<div/>')
    .attr('id', 'column2C')
    .css({
            width: '50%',
            height: '154px',
            float: 'left',
            margin: 0,
            'margin-bottom': '16px'
    }).hide().appendTo($columnsB);    
    
    var $propFieldSet = $('<fieldset/>')
      .append('<legend>Name New Properties</legend>');
    $column2C.append($propFieldSet);
    
    var $p1 = $('<p>p1(x,y):<input id = "p1text" type="text"/></p>')
      .css('margin-top', '6px')
      .css('margin-bottom', '6px');
    $p1.appendTo($propFieldSet);
    var $p2 = $('<p>p2(y,z):<input id = "p2text" type="text"/></p>')
      .css('margin-bottom', '6px');
    $p1.after($p2);
    
    var $children = $('<ul></ul>')
      .attr('id', 'children')
      .css('list-style-type', 'none')
      .css('height', '100px')
      .css('width', '180px')
      .css('overflow-y', 'auto');
      
    $parentMenu = $('<select/>')
      .attr('id', 'parMen')
      .css('width', '190px')
      .css('margin-top', '6px')
	   .change(function(){
	      $children.empty();
	      var this_parent;
	      if ($('#parMen option:selected').text() != '--Select Parent Node--') {
	            var node = get_node_ext($('#parMen option:selected').text());
	            this_parent = get_parent_ext($('#parMen option:selected').text());
		    	  if(node.collapseVictim){
		    		  var updating_collapsed = node;
		    		  while(updating_collapsed.name != "owl:THING" && updating_collapsed.collapseVictim ) {
		    			  if (updating_collapsed.isCollapsed) {
		    				  updating_collapsed.isCollapsed = false;
		    				  updating_collapsed.fixed = false;
		 	    		  }
		    			  updating_collapsed = get_parent_ext(updating_collapsed.name);
		    		  }
		    		  if (updating_collapsed.isCollapsed && updating_collapsed.name == "owl:THING") {
		    		    updating_collapsed.isCollapsed = false;
		    		    victimize_children(updating_collapsed);
		    		  }else{
		    			 updating_collapsed.isCollapsed = false;
		    			 victimize_children(updating_collapsed);
		    		  }
		    		  update();
		    	  }
	    	  if(this_parent != null) d3.select('#importantg').transition().attr("transform", "scale("+2+","+2+") translate("+(350-node.x)+","+(170-node.y)+")");

	      }
	      all_nodes.forEach(function (node) {
	        if ((node.name == $('#parMen :selected').text()) && ($('#parMen :selected').text().substring(0,4) != 'xsd:')) {
	    	  node.children.forEach(function (child) {
    	        var $li = $('<li/>');
    	        var $span = $('<span/>').attr('title', child.name)
    	          .append($li);
    	        $children.append($span);
    	        var $ch = $("<input type = 'checkbox' class='radioLike'/>");
    	        $li.append($ch);
    	        if(child.name.length <= 16){
    	          $li.append(child.name);
    	        } else {
    	          $li.append(child.name.substring(0,15) + '...');
    	        }
	    	  });
	        }
	    });
	  });

      $parentMenu.appendTo($column1);
      $parentMenu.append('<option>--Select Parent Node--</option>')
      var $parentMenuSpan = $('<span>Select Parent Node:</span>');
      $parentMenu.before($parentMenuSpan);
      
      var $properties = $('<ul></ul>')
        .attr('id', 'properties')
        .css('list-style-type', 'none')
        .css('height', '100px')
        .css('width', '180px')
        .css('overflow-y', 'auto');
      
      $sourceMenu = $('<select/>')
  	    .attr('id', 'sourceMen')
  	    .css('width', '190px')
  	    .css('margin-top', '6px')
        .change(function() {
    	  $properties.empty();
    	  all_nodes.forEach(function (node){
    		  all_links.forEach(function (link) {
        		  if (link.source.name == $('#sourceMen :selected').text() && link.source.name == node.name) {
        			  	  var $li = $('<li/>');
    	                  var $span = $('<span/>').attr('title', link.propname)
    	                    .append($li);
    	                  $properties.append($span);
    	                  var $ch = $("<input type = 'checkbox' class='radioLike'/>");
    	                  $li.append($ch);
    	                  if(link.propname.length <= 16){
    	                	$li.append(link.propname + " : " + link.target.name);
    	                  }else{
    	                	$li.append(link.propname.substring(0,15) + '... : ' + link.target.name);
    	                  }
        			  }
        			  $('#legendWords').text('Instantiate Subclasses');
        		  });
    	  	  });
    	  });
      

      $sourceMenu.hide().appendTo($column1);
      $sourceMenu.append('<option>--Select Source Node--</option>');
      var $sourceMenuSpan = $('<span>Select Source Node:</span>').hide();
      $sourceMenu.before($sourceMenuSpan);
      
      $properties.css('padding-start', '0px')
        .hide().appendTo($column1);
      
      $children.css('padding-start', '0px')
      	.appendTo($column1);
      
      all_nodes.forEach(function (node) {
    	  if (node.name != 'owl:THING' && node.children) {
    		  $parentMenu.append('<option>' + node.name + '</option>');
    	  }
      });
      all_nodes.forEach(function (node) {
    	   var exists = false;
    	   all_links.forEach(function(link){
    		   if(link.source.name == node.name && link.target.name == 'xsd:string'){
    			   exists = true;
    		   }
    	   });
    	   if(exists){
    		   $sourceMenu.append('<option>' + node.name + '</option>');
    	   }
      });

      var $column2 = $('<div/>')
      .attr('id', 'column2')
      .css({
      		width: '50%',
      		height: '154px',
      		'margin-bottom': '16px',
      		float: 'right'
      }).appendTo($columns);
      
      var $textArea = $('<form/>').appendTo($column2);
      var $textField = $('<fieldset/>')
        .css('width', '196px')
        .appendTo($textArea);
      var $inputLegend = $('<legend/>').appendTo($textField);
      $inputLegend.append("<p id='legendWords'>Define Property Relation</p>");
      var $textInput = $('<input/>')
        .attr('id','textInput')
        .css('margin', '1px').appendTo($textField);
      var $addButton = $('<button/>')
        .attr('id', 'addButton')
        .css('margin', '1px')
        .text('add').hide().appendTo($textField);
      
      var $subclasses = $('<ul></ul>')
        .attr('id', 'subclasses')
        .css('list-style-type', 'none')
        .css('height', '100px')
        .css('width', '224px')
        .css('padding-left', '0px')
        .css('margin-right', '5px')
        .css('overflow-y', 'auto').appendTo($column2);
    
      
      $enumerationRadio.change(function() {
          if (this.checked) {
              //Fade out attribute things.
        	  $columnsB.hide();
        	  $radio3.hide();
        	  $radio2.fadeIn();
	    	  $('#sources1B option:first').attr('selected', true);
	          $('#inMenu option:first').attr('selected', true);
	          $firstProps.empty();
	          $firstProps.append('<option>--Select First Property--</option>');
	          $firstProps.attr('disabled', 'true');
	          $('#outMenu option:first').attr('selected', true);
	          $secondProps.empty();
	          $secondProps.append('<option>--Select Second Property--</option>');
	          $secondProps.attr('disabled', 'true');
	          $('#nodesMenu option:first').attr('selected', true);
	      	  $('#sourceNodes option:first').attr('selected', true);
	          $('#propertiesMenu option:first').attr('selected', true);
	      	  $propertiesMenu.empty();
	      	  $propertiesMenu.append('<option>--Select a Property--</option>');
	      	  
	      	$propertiesMenu.attr('disabled', 'true');
	    	$nodesMenu.attr('disabled', 'true');
        	  $columns.fadeIn();
          }
      });
      
      $attributeRadio.change(function() {
          if (this.checked) {
        	//Fade out attribute things.
        	  $columns.toggle();
        	  $properties.empty();
        	  $children.empty();
        	  $subclasses.empty(); transportList = [];
        	  $('#parMen option:first').attr('selected', true);
        	  $('#sourceMen option:first').attr('selected', true);
        	  $radio2.hide();
        	  $radio3.fadeIn();
        	  
        	//Fade in enumeration things.
        	  $columnsB.fadeIn();
          }
      });
      
      // add SUBMIT button
      var $submit = $('<input/>')
          .attr("type", "submit")
          .attr("value", "SUBMIT")
          .css('float', 'left')
          .on("click", function() {
        	  d3.select('#importantg').transition().attr("transform", "scale("+1+","+1+")");
          }) 
          .css("margin-bottom", "6px").appendTo($change);

      // add CANCEL button to outer div to avoid triggering inner form
      div_tmp.append("button")
        .text("CANCEL")
        .on("click", function() {
          d3.select("#info").remove();
          d3.select('#importantg').transition().attr("transform", "scale("+1+","+1+")");
        });
    }
  }

  });
});