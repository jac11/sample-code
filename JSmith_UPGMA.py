#!/usr/bin/python
# Jaclyn Smith
# 08.10.14
# UPGMA distance matrix
# and phenogram construction
# requires fasta file input

import sys

# find minimum value in matrix
def find_min(matrix):
	minumS = 0
	minumT = 1
	min_val = matrix[0][1][1]
	for i in range(len(matrix)):
		for j in range(i+1,len(matrix)):
			if matrix[0][1][j] != 0.0:
				if matrix[i][1][j] < min_val :
					min_val = matrix[i][1][j]
					minumS = i
					minumT = j
	return (minumS, minumT)

def UPGMA(infile):
	aligns = {}
	distances = []
	species = []
	pos = ''

	# fill data structures
	for line in infile:
		line = line.strip('\n')
		if ">" in line:
			pos = line[1:]
			distances.append([pos])
			species.append(pos)
			aligns[pos] = ''
		else:
			aligns[pos] += line

	# establish matrix
	dis = [0.0]*len(distances)
	for i in range(len(distances)):
		distances[i].append([0.0]*len(distances))


	# calc sequence identity %
	for i in range(len(species)):
		for j in range(len(species)):
			seqid = 0.0
			for k in range(len(aligns[species[i]])):
				if aligns[species[i]][k] != aligns[species[j]][k]:
					seqid += 1.0
			distances[i][1][j] = round(seqid/len(aligns[species[i]]),4)

	count = 1
	# collapse matrix until two clades remain
	while len(distances) > 2:
	
		# get distances
		min_cords = find_min(distances)
		side = min_cords[0]
		top = min_cords[1]

		# remove from species list, create merged entry
		dist = distances[side][1][top]
		new_name = "(" + str(species[side]) + ":" + str(dist/2) + "," + str(species[top]) + ":" + str(dist/2) + ")"
		species.remove(species[side])
		species.remove(species[top-1])
		species.append(new_name)

		# remove from matrix
		side_val = distances.pop(side)
		side_val[1].pop(side)
		side_val[1].pop(top-1)
		top_val = distances.pop(top-1)
		top_val[1].pop(side)
		top_val[1].pop(top-1)

		# calc new matrix
		new_entry = [new_name, []]
		for i in range(len(side_val[1])):
			tmp = round((((side_val[1][i] + top_val[1][i]) - dist)/2), 4)
			new_entry[1].append(tmp)
			distances[i][1].append(tmp)
		
		# print new_entry
		new_entry[1].append(0.0)
		for l in distances:
		 	l[1].pop(side)
			l[1].pop(top-1)
		distances.append(new_entry)
		count += 1

	# construct final tree with last two clades
	dist = distances[0][1][1]
	new_name = "(" + str(species[0]) + ":" + str(dist/2) + "," + str(species[1]) + ":" + str(dist/2) + ")"
	print "\nFinal Tree (Newick Format) \n"
	print new_name, "\n"

def main(argv=None):
	if argv == None:
		argv = sys.argv[1:]
	try:
		# fasta input
		readFile = open(argv[0], "r")
	except:
		err = "Fasta input file required."

	UPGMA(readFile)

if __name__ == "__main__":
	sys.exit(main())